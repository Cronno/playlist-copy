import sys
import shutil
from pathlib import Path
from urllib.parse import quote, unquote

def displayHelp():
    print("Usage: python3 "  + sys.argv[0] + " playlist_file dest_dir [options]")
    print("[options]")
    print("\t-e : the output playlist will have URL encoding")
    print("\t-o : overwrite files if they already exist")
    print("\t-r : do not copy directory structure, put all files in destination root")
    print("\t-a : the output playlist uses absolute paths")
    sys.exit()

def processLine(line):
    #decode the line in case it is encoded
    line = unquote(line)
    #cut off the \n
    line = Path(line[:-1])
    return line

def copyFile(src, dest, overwrite):
    if not dest.parent.is_dir():
        dest.parent.mkdir(parents=True)
    if not dest.exists() or overwrite:
        printName(src)
        shutil.copy2(src, dest.parent)
        #erase the line in the terminal
        sys.stdout.write("\033[K")

def printName(src):
    outname = str(src.name)
    if len(outname) > 50:
        outname = outname[:47] + "..."
    print("Copying " + outname, end="\r", flush=True)

def getDestination(dest_base, src_base, line, root):
    if root:
        return dest_base/line.name
    if line.is_absolute():
        line = line.relative_to(src_base.resolve())
    return dest_base/line

def outputLine(destPlaylist, line, dest_base, encode, absolute):
    if absolute:
        line = line.resolve()
    else:
        line = line.relative_to(dest_base)
        #if path is not absolute use forward slashes
        line = line.as_posix()
    if encode:
        destPlaylist.write(quote(str(line), safe='\\/')+'\n')
    else:
        destPlaylist.write(unquote(str(line))+'\n')

encode = False
overwrite = False
root = False
absolute = False
#Check arguments
if len(sys.argv) < 3 or "-h" in sys.argv or "--help" in sys.argv:
    displayHelp()
elif len(sys.argv) > 3:
    if "-e" in sys.argv[3:]:
        encode = True
    if "-o" in sys.argv[3:]:
        overwrite = True
    if "-r" in sys.argv[3:]:
        root = True
    if "-a" in sys.argv[3:]:
        absolute = True
    
if sys.argv[1].split('.')[-1] not in ["m3u","m3u8"]:
    print("Error: " + sys.argv[1] + " is not a .m3u or .m3u8 file")
    sys.exit()
    
#The base directories for the source and destination locations
src_base = Path(sys.argv[1]).parent
dest_base = Path(sys.argv[2])

#If the destination does not exist, create it
if not dest_base.exists():
    dest_base.mkdir(parents=True)

#open files
file = open(sys.argv[1], 'r', encoding="utf-8-sig")
filename = Path(sys.argv[1]).name
destPlaylist = open(dest_base/filename, 'w', encoding="utf-8")

count = 0
errors = 0
for line in file:
    #handle tag lines
    if line[0] == '#':
        destPlaylist.write(line)
        continue
    line = processLine(line)
    src = src_base/line
    if src.exists():
        dest = getDestination(dest_base, src_base, line, root)
        copyFile(src, dest, overwrite)
        outputLine(destPlaylist, dest, dest_base, encode, absolute)
        count += 1
    else:
        print("File not found: " + str(src))
        errors += 1
file.close()
destPlaylist.close()

print(str(count) + " Files from playlist \"" + sys.argv[1] + "\" were copied to \"" + sys.argv[2] + "\"")
if errors > 0:
    print("Errors: " + str(errors))
