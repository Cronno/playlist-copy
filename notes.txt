winamp
    not encoded
    utf-8-bom
    relative paths
    
vlc
    encoded (will read both)
    utf-8
    relative paths
    forward slashes
    
foobar2000
    not encoded
    utf-8-bom
    relative paths
    no # lines (except first line)
    
musicbee
    not encoded (will read both)
    utf-8
    absolute paths (optional), relative paths begin with .\
    no # lines
    .m3u
    
itunes
    not encoded (will read both)
    utf-8
    absolute paths (will read both)
    
aimp
    not encoded
    utf-8-bom
    absolute paths (optional)
    
audacious
    not encoded
    utf-8
    relative paths
    no # lines
    .m3u
    
clementine, strawberry
    not encoded
    utf-8
    relative paths
    forward slashes
    
media monkey
    not encoded
    utf-8
    relative paths
    no # lines

mpc
    not encoded
    utf-8
    absolute paths (will read both)
    no # lines